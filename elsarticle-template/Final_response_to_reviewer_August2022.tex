\documentclass[11pt]{article}

% Any additional packages needed should be included after jmlr2e.
% Note that jmlr2e.sty includes epsfig, amssymb, natbib and graphicx,
% and defines many common macros, such as ‘proof’ and ‘example’.
%
% It also sets the bibliographystyle to plainnat; for more information on
% natbib citation styles, see the natbib documentation, a copy of which
% is archived at http://www.jmlr.org/format/natbib.pdf


\usepackage{fullpage}
\usepackage{color}
\usepackage{epstopdf}
\usepackage{amsmath,amstext,amsfonts,amssymb,bbm,float,amsthm}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{enumerate}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{bbm}
\usepackage{float}

\input{../LAA/final_revision/everything.tex}

\newcommand{\dt}{{\textcolor{black}{\delta_t}}}
\newcommand{\da}{{\textcolor{black}{\delta}}}

%\linespread{2}
\usepackage{xparse}
%\newtheorem{remark}[theorem]{Remark}
%\newtheorem{example}[theorem]{Example}

% Definitions of handy macros can go here

\newcommand{\dataset}{{\cal D}}
\newcommand{\fracpartial}[2]{\frac{\partial 1}{\partial  2}}

% Heading arguments are {volume}{year}{pages}{submitted}{published}{author-full-names}


\begin{document}

\title{Response to the reviews \\ {\bf On the Convergence Analysis of Asynchronous SGD for Solving Consistent Linear Systems} \\ LAA-D-20-00370R4:  Linear Algebra and its Applications}
\date{}
\maketitle

We thank the reviewer and the editor for their efforts in reviewing our paper. The editor mentioned ``The theorems seems to say, for the algorithm APSGD if it runs for $T \geq$ some number of steps (as a function of $\delta$ and $1/\epsilon$), then,
the expected value of the square of the norm of the error (in a particular $B$-norm) is less than or equal to $\epsilon$ times quantity. This makes perfect sense." However, the editor further noted, ``the additional inequality
saying that the function of $\delta$ and $1/\epsilon (\xi_{\rm opt})$ is bounded by above by some quantity (as the referee pointed out)." The editor thinks there could be a logical issue. 

Previously, we changed the statements of Theorem 5 and 6 to mitigate any further confusion the reviewer might have about them. We reported that there was a misunderstanding from the reviewer's side with the inequality---We found no fault. Moreover, results of this genre are widespread in optimization literature that associates the iteration complexity with the convergence of the iterates of an algorithm; in our case, the quantity is $\E{\|x_T-x_\star\|_{\mB}}^2.$  We believe that issue is mitigated as the reviewer did not comment further on the inequalities.  

We thoroughly reexamined the results, and humbly note that we did not find any logical flaw. Therefore, we respectfully disagree with the statement ``There seems to be a logical issue here", but we realized that the results can be better clarified, as the editor suggested, and we will address the confusion. First, we apologize for this confusion and thank the reviewer/editor for pointing it out. In what follows, we realized the issue might possibly be calling $\chi_{a_{opt}}(\delta)$ the {\bf optimal iteration complexity} first, and then defining it afterwards. So, it is not clear what comes first, the statement that it is optimal, or the definition. To mitigate this, we consider, $\chi_{a_{opt}}(\delta)$ simply an expression we clearly define, without giving it any name at that point; see (35) in the revised version of the paper. We then say what the definition implies. We did not call it the {\bf optimal iteration complexity} at this point, as that indeed looks like a circular reasoning. We realized the reader might grasp the idea of the {\bf optimal iteration complexity} is what defines this concept, whereas the definition is nothing but a simple expression, which is numbered (8) in this response ((35) in the revised paper). In light of this change, the first result, as in Theorem 5 is a consequence of optimal choice of parameters, ($\theta$, $\omega$) for \textbf{Case 1}, while the second result, as given in Theorem 6 is for a good combination of ($\theta$, $\omega$) for \textbf{Case 2}, and they both provide an upper bound on the quantity, $\chi_{a_{opt}}(\delta)$, hence directly implying for both cases, it is optimal. %We also added this as a new remark; please see Remark 5 in Section 5 of the revised paper. 

Nevertheless, we understand that these are nontrivial theoretical results that are not easy to comprehend unless one knows the entire structure and motivation behind where and how they come from. In this rebuttal, we give a brief outline of that. We respectfully note that we revised the paper accordingly. %Moreover, after reading our rebuttal, if the reviewer/editor finds a better direction to present the results and has objective remarks and feedback to improve them, we will gladly address them in our revised version. \textbf{To conclude, we respectfully note that, from our perspective, there are no logical or mathematical flaws. The reviewer has repeatedly mentioned it in the past 4 reviews.} 
Please see below our response in creating a {\em high-level overview} to present the results in Theorem 5 and 6. We hope this will help the reviewer/editor to see the big picture and fully comprehend the results. %We respectfully note that to improve the paper's readability, we previously added a Section ``Overview of analysis"; please see Section 4.1; we can add a few more details if the reviewer/editor insists. Admittedly, we can always clarify further, but as mathematicians, we also need to be aware of whether we are diluting our results; some readers do not appreciate that either.  


%We understand the confusion. 

\section*{Answer to Reviewer}
Let $p_t=\E{\|x_t-x_\star\|_{\mB}^2}$. In the analysis, we handle the following recurrence relation
\begin{equation}\label{rec}
	p_{t+1}\leq a\ p_t+b\ p_{t-\dt}, \;\;{\rm for\;all\;} t \geq 0,
\end{equation}
where $a\geq0$, $b\geq0$, and $a+b<1$; see Theorem~2 for this result. Let $\delta$ be the maximum delay, we present the recurrence relation \eqref{rec} in the following form:
\begin{eqnarray}\label{state_transition}
u_{t+1}\leq
\mA_{\dt}u_t,
\end{eqnarray}
where  $u_t =(p_{t}\;\;p_{t-1}\cdots p_{t-\da})^\top$, and $\mA_{\dt}\in\R^{(\da+1)\times(\da+1)}$ is a state transition matrix, such that $\mA_{\dt} = \begin{pmatrix}
&[a\;\;\mathbf{0}_{\dt - 1}^\top] & b  \quad [0_{\da-\dt}^\top]\\
&I_{\da} &\mathbf{0}_{\da}\end{pmatrix}$. The next important result that comes after this is Lemma 5 that says ``The rate of convergence of the recurrence in \eqref{rec} is given by the spectral radius, $\rho$, of the state transition matrix, $\mA_\da$." The characteristic polynomial of the state transition matrix, $\mA_\da$ turned out to be:
\begin{equation}\label{characteristic_poly}
   p_{\da}(\gamma) \eqdef \gamma^{\da+1}  - K_1(\theta,\omega) \gamma^\da - K_2(\theta,\omega), 
 \end{equation}
Finding a closed form analytic expression for the unique positive root of \eqref{characteristic_poly} is not possible. Therefore, we find a polynomial that bounds $p_{\da}(\gamma)$ on the interval $[0,1]$. 
This leads us to finding the polynomial which bounds $p_{\da}(\gamma)$ from below on the interval $[0,1]$, and its positive root can be easily found in a closed form, and serves as an upper bound to the unique positive root of $p_{\da}(\gamma)$.~This leads to the following Lemma (Lemma 6 in the main paper). 
\begin{lemma}\label{lem:kjd}
The polynomial,  
\begin{equation}
 g_{\da}(\gamma) \eqdef \left(1+\frac{1}{\da}-K_1\right)\gamma^\da  - \left(K_2+\frac{1}{\da}\right),
 \end{equation}
bounds the characteristic polynomial $p_{\da}(\gamma)$ from below on $[0,1]$ and its root
\begin{equation}\label{upper_bound}
    \beta(\theta,\omega,\da)=\left(\frac{K_2+\frac{1}{\da}}{1-K_1+\frac{1}{\da}}\right)^{\frac{1}{\da}}
\end{equation}
 %$\left(\frac{K_2+\frac{1}{\dt}}{1-K_1+\frac{1}{\dt}}\right)^{\scalebox{1.2}{$\frac {1}{\dt}$}}$  
  is an upper bound to the unique positive root  of $p_{\da}(\gamma)$.
\end{lemma}
Let $\beta_{a_{\rm opt}}$ denote the rate of convergence of APSGD with optimally tuned $\theta$ and $\omega$. That is,
\begin{equation}\label{ub rho opt}
    \beta_{a_{opt}}(\da) = \min_{\theta,\omega}\left(\frac{K_2+\frac{1}{\da}}{1-K_1+\frac{1}{\da}}\right)^{\frac{1}{\da}}.
\end{equation}
{Let 
\begin{equation}\label{eq:pd}
P_\da=\max\left(\|x_{\da}-x_\star\|_{\mB}^2, \beta_{a_{opt}}(\da)\|x_{\da-1}-x_\star\|_{\mB}^2, \hdots,\beta_{a_{opt}}(\da)^\da \|x_0-x_\star\|_{\mB}^2\right).
\end{equation}
Then, we obtain the following convergence guarantee
\begin{equation*}
\E{\|x_T-x_\star\|_{\mB}^2} \leq  \beta_{a_{opt}}(\da)^{T-\da} P_\da .
\end{equation*}

Let
\begin{equation}\label{eq:olpa}
    \chi_{a_{opt}}(\da)\eqdef\frac{\da}{1-(\beta_{a_{opt}})^{\da}}\geq \frac{\da}{\log(\beta_{a_{opt}}^{-\da})}=\frac{1}{\log(\beta_{a_{opt}}^{-1})},
\end{equation}
then if
\begin{equation}\label{eq:poir}
T-\da\geq \chi_{a_{opt}}(\da)\log(\frac{1}{\epsilon}),\; {\rm\;then\;this\;implies\;} \E{\|x_T-x_\star\|_{\mB}^2} \leq \epsilon P_\da.
\end{equation}}
Plugging \eqref{ub rho opt} in \eqref{eq:olpa}, we have
\begin{equation}
    \chi_{a_{opt}}(\da) = \da \cdot \min_{\theta,\omega}\left(\frac{1-K_1+\frac{1}{\da}}{1-K_1-K_2}\right).
\end{equation}
Denote
\begin{equation}\label{defU}
    U(\theta,\omega)= \frac{1-K_1+\frac{1}{\da}}{1-K_1-K_2}.
\end{equation}
Therefore,
\begin{equation}\label{eq:optimal iter compl}
    \chi_{a_{opt}}(\da) = \da \cdot \min_{\theta,\omega}U(\theta,\omega).
\end{equation}

Note that, the above expression for $U(\theta,\omega)$ is feasible only when $K_1+K_2<1$. We now only remain to optimize for $U(\theta,\omega)$ to derive our convergence result. The first result, as in Theorem 5 is a consequence of optimal ($\theta$, $\omega$) for \textbf{Case 1}, while the second result, as given in Theorem 6 is for a good combination of ($\theta$, $\omega$) for \textbf{Case 2}; see both cases in Figure 1 (this is Figure 2 in the main paper). The different inequalities on $\beta(\theta,\omega,\da)$ is an direct artifacts of the two cases mentioned above. Please see this as a new remark, Remark 5 in Section 5 of the revised paper. 
\begin{figure}
    \centering
    \begin{subfigure}{0.49\textwidth}
    \includegraphics[width=\textwidth]{../Figures/Case1.pdf}
        \caption{}
      \label{1a}
      \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
    \includegraphics[width = \textwidth]{../Figures/Case2.pdf}
    \caption{}   
    \label{1b}
  \end{subfigure} 
 \caption{\small{The region of interest $(\theta, \omega)=[0,1]\times [1,\omega^\star] $ is shaded-- (a) Case 1: $\omega^\star\leq 2$, that is, $\lambda^+_{\min}+\lambda_{\max} \geq 1$, (b) Case 2: $\omega^\star\geq 2$, that is, $\lambda^+_{\min}+\lambda_{\max} \leq 1$.}}
    \label{roi}
\end{figure}
Hence the final statements of the theorems are presented as follows:
\begin{enumerate}
\item \textbf{Theorem 5.}
Let $\lambda_{\min}^+ + \lambda_{\max}\geq1$. The quantities, $\chi_{a_{opt}}(\da), P_\da$, $U(\theta_1,1)$, and $U(\theta_{\omega^\star},\omega^\star)$ are given in equations (37), (34), (56), and (59), respectively. If APSGD (Algorithm 1) runs for
$$T\geq \da \cdot \min\left(U(\theta_1,1),U(\theta_{\omega^\star},\omega^\star)\right) \log(\frac{1}{\epsilon})+\da \geq \chi_{a_{opt}}(\da)\log(\frac{1}{\epsilon})+\da$$  iterations, then we have $\E{\|x_T-x_\star\|_{\mB}^2} \leq \epsilon P_\da.$
\item \textbf{Theorem 6.} 
Let $\lambda_{\min}^+ + \lambda_{\max}\leq1$. The quantities, $\chi_{a_{opt}}(\da)$ and $P_\da$ are given in equations (37) and (34), respectively. If APSGD (Algorithm 1) runs for
$$
T\geq \frac{\frac{1}{4}+\frac{\lambda_{\min}^+}{2}+\frac{1+\sqrt{\da+1}}{2\da}}{\lambda_{\min}^+}\da\log(\frac{1}{\epsilon})+\da \geq \chi_{a_{opt}}(\da)\log(\frac{1}{\epsilon})+\da$$ iterations, then we have $ \E{\|x_T-x_\star\|_{\mB}^2} \leq \epsilon P_\da.$

\end{enumerate}

%\paragraph*{Major comments.}
%\begin{enumerate}
%\item {\it  The analysis holds only when the recurrence is stable and does not hold when the delay is time-varying. {More generally a uniform bound on the eigenvalue magnitudes does not obviously give a rate of convergence in the time-varying case.}}
%
%\textbf{Answer.} 
%We sincerely thank the reviewer for this insightful comment. We note that we only require a bounded delay for the convergence analysis of APSGD. The key result which helps us achieve this is Lemma 6 in the current version of the paper~(Lemma 5 in the previous version), which states that the bound on eigenvalues of the state transition matrix $A$ is monotone in delay. Specifically, the rate is monotone in delay---the higher the delay, the higher the bound is. This allows us to simply upper bound an arbitrary delay's rate by the rate for maximum $\delta_a$ delay. Lemma 6 is now a fundamental result in establishing the convergence of APSGD.
%Moreover, the convergence analysis of APSGD is now a standalone result that can handle any time-varying bounded delay that the reviewer was concerned. To elaborate more, the convergence analysis of our proposed APSGD can handle any variable delay, $\dt$ bounded by a maximum delay of $\da$. Additionally, we have now added an ``Overview of our analysis" to help the reader better understand the recurrence notion that appears in the form of a state-transition system and our proof techniques that at places become rigorous. To conclude, by disentangling the convergence analysis of APSGD and its comparison with SPSGD, the results in Section 5 now require only a bounded delay $\delta_a$ that is the upper bound of any time-varying delay as mentioned by the reviewer.
%\end{enumerate}
%
%\paragraph*{Minor comments}
%
%\begin{enumerate}
%
%\item{\it The authors describe a “unit time interval” to mean the maximum delay for all processors to report $(\delta_a)$; since time is measured in terms of updates at the master, $\delta_a>\tau$ (the number of processors), so a “unit time interval” does not actually correspond to a unit of time. I would recommend choosing different terminology.}
%
%\textbf{Answer.} We thank the reviewer for pointing this out.~We have removed using ``unit time interval" and now clearly state our setup to compare APSGD and SPSGD and justify why this setup is meaningful.
%We agree with the reviewer that the notion of the ``unit-time interval" that we were considering in our previous analysis to compare the iteration complexities was confusing. By removing the idea of ``unit-time interval" in our present version, we alleviate this issue. As mentioned before, by eliminating the notion of ``unit time interval" and disentangling the analysis of APSGD and its comparison with SPSGD, the convergence of APSGD can handle {\em any time-varying bounded delay}. Although analyzing different delays of APSGD under diverse setups is a mathematically complicated problem and not in the scope of this paper. 
%
%
%\item{\it “For the sake of simplicity, we assume the number of updates by the asynchronous parallel SGD in a unit-time interval to be constant throughout the execution of the algorithm”--- this is weaker than the assumption that the authors actually make in their analysis, but is still probably stronger than appropriate. It’s unclear whether arguments made with this assumption would actually generalize to the case of interest (which is just a bounded delay $\delta_a$).}
%
%\textbf{Answer.} 
%Any comparison we make between APSGD and SPSGD requires a notion of time equivalence between their updates. Hence, we are required to assume more than just a bounded delay to make any comparison. As we mentioned in Section 5 of the revised version, a completely lock-free APSGD has both {\em intra-iteration} and {\em inter-iteration} gain compared to its synchronous counterpart SPSGD. However, our setting only depicts inter-iteration gain. So, we find our setting more restrictive towards APSGD than what one would expect in practice. Recall that SPSGD takes one step after averaging $\tau$ gradient computations, one by each of the $\tau$ worker.~In contrast, APSGD takes one step after every gradient computation. To make a comparison between SPSGD and APSGD, we assume time equivalence between one step of SPSGD and $\delta_a$ steps of APSGD. Please see Figure 1 added in the manuscript. This setting is a depiction of distributed setups with predictable but variable computing time of the nodes. We note that under this setting, one can consider the {\em intra-iteration gain of APSGD} due to the synchronization barrier after every $\delta_a$ steps. Therefore, APSGD is expected to perform even better without the synchronization barrier, but we do not consider that in our setting. 
%
%\item{\it The use of $S_i$ to denote the number of updates in the given interval from processor $i$ is unfortunate given the use of a bold $S$ elsewhere for the samples drawn at each step; I suggest simply using a different letter. }
%
%\textbf{Answer.} Many thanks for noticing this! We removed this notation in the revised version of our paper.
%
%\item{\it I also found it irritating to flip back and forth to Appendix A for the proofs. None of the proofs is all that long, and I think it would be better to just include them inline. }
%
%\textbf{Answer.} Thank you very much for this suggestion. We have added the proofs inline.
%
%
%
%\end{enumerate}
%
%
%\paragraph*{Text modifications.}
%Below we note the major restructuring in the revised version. Major modifications are indicated in \textcolor{blue}{blue} in the revised version. 
%
%\begin{enumerate}
%\item{Parallel basic algorithm changed to synchronous parallel SGD~(SPSGD) algorithm in order to change confusion with the basic method. This lead us to APSGD and SPSGD as abbreviations.}
%\item We added a simple example to demonstrate the setting to compare between our asynchronous parallel SGD~(APSGD) and synchronous parallel SGD~(SPSGD) proposed by Richt\'{a}rik and Tak\'{a}\v{c} in \cite{richtarik2017stochastic}. 
%\item{We have now removed the weak-convergence result~(Previously Section 4.1) and modified the text to only focus on strong convergence. We note that, weak-convergence is a mere consequence of the strong convergence. This keeps the merit of the paper intact.}
%\item{For better readability, we added a new Section called ``Overview of analysis"---Section 4.1.}
%\item {We propose the convergence analysis of asynchronous parallel SGD~(APSGD) in Section 4 (previously Section 5), and Section 5 is dedicated for its comparison with synchronous parallel SGD (SPSGD). We dispensed the need of a ``unit time interval” from our analysis.}    
%\item Tables are presented in a more compact form. 
%\item{Newer references were added and rewording of the references in abstract and introduction.}
%\end{enumerate}
%
%
%



\bibliographystyle{plain}
\bibliography{../references_asyn}










\end{document}



