clear all;

%Case 1: when lmin + lmax >= 1

%ASGD is always asymptotically better

lmin = [0.1;0.01;0.001;0.0001;0.2;0.1;0.01;0.001;0.0001;0.2;0.1;0.01;0.001;0.0001;0.2;];
lmax = [0.9;0.99;0.999;0.9999;0.8;0.9;0.99;0.999;0.9999;0.8;0.9;0.99;0.999;0.9999;0.8;];
c = [1;1;1;1;1;1.5;1.5;1.5;1.5;1.5;2;2;2;2;2];
k = lmin + lmax;
cond_num = lmax./lmin;

T = table(lmin, lmax, c, k,cond_num);

tau = ones(height(T),1);

for i=1:height(T)
    tau(i) = computeTauC1(T.lmin(i), T.lmax(i), T.c(i));
end

T.tau = tau