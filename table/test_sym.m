syms f(tau)
c = 2;
lmax = 0.35;
lmin = 0.1;
f(tau) = 1/tau + (1-1/tau)*lmax == 1/4 + lmin/2 + (1+(c*tau+1)^(1/2))/(2*c*tau);
assume(tau,'real')
soltau = vpasolve(f, tau, [0,10000], 'random', true)