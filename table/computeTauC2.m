function tauCross = computeTauC2(lmin,lmax,c)


%Case 2: when lmin + lmax lies in [0,1]

%ASGD is asymptotically better when 1/4 + lmin/2 <= lmax

syms f(tau)

iterSync = 1/tau + (1-1/tau)*lmax;
iterAsync = 1/4 + lmin/2 + (1+(c*tau+1)^(1/2))/(2*c*tau);
f(tau) =  iterSync - iterAsync;

tauCross = ceil(findzeros(f,[1,10000]));
return
