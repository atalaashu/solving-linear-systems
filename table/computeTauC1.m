function tauCross = computeTauC1(lmin,lmax,c)

%Case 1: when lmin + lmax >= 1

%ASGD is always asymptotically better

syms f(tau)

k = lmin + lmax;

iterSync = 1/tau + (1-1/tau)*lmax; 
asyncTerm1 = (3*lmin + lmax)/4 + 1/(2*c*tau);
asyncTerm2 = (c*tau + 1 +c*tau*(1-k)*lmin)/(c*tau*(1+c*tau*(2-k))^0.5);
iterAsync =  asyncTerm1 + asyncTerm2;

f(tau) =  iterSync - iterAsync;

tauCross = ceil(findzeros(f,[0,10000]));
return

