\documentclass[11pt]{article}

% Any additional packages needed should be included after jmlr2e.
% Note that jmlr2e.sty includes epsfig, amssymb, natbib and graphicx,
% and defines many common macros, such as ‘proof’ and ‘example’.
%
% It also sets the bibliographystyle to plainnat; for more information on
% natbib citation styles, see the natbib documentation, a copy of which
% is archived at http://www.jmlr.org/format/natbib.pdf


\usepackage{fullpage}
\usepackage{color}
\usepackage{epstopdf}
\usepackage{amsmath,amstext,amsfonts,amssymb,bbm,float,amsthm}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{enumerate}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{bbm}
\usepackage{float}

\input{everything.tex}

\newcommand{\dt}{{\textcolor{orange}{\delta_t}}}
\newcommand{\da}{{\textcolor{red}{\delta}}}

%\linespread{2}
\usepackage{xparse}
%\newtheorem{remark}[theorem]{Remark}
%\newtheorem{example}[theorem]{Example}

% Definitions of handy macros can go here

\newcommand{\dataset}{{\cal D}}
\newcommand{\fracpartial}[2]{\frac{\partial 1}{\partial  2}}

% Heading arguments are {volume}{year}{pages}{submitted}{published}{author-full-names}


\begin{document}

\title{Response to the reviews \\ {\bf On the Convergence Analysis of Asynchronous SGD for Solving Consistent Linear Systems} \\ LAA-D-20-00370 :  Linear Algebra and its Applications}
\date{}
\maketitle

We would like to sincerely thank the reviewer for his/her careful summaries and suggestions. Especially, we thank for noting that {\em ``The major contribution of this paper is the convergence analysis, leading to a rather remarkable claim that the asynchronous version converges faster than
the synchronous variant even in the case when the synchronous variant does not suffer from idle time due to load imbalance within phases."} All the comments provided are immensely helpful, and our revision based on them strengthened the paper. Below please find our response to all comments. 


\section*{Answer to Reviewer}

We sincerely thank the reviewer for pointing out some interesting details in the theoretical part of the paper. We mention that inspired by the reviewer’s comments; we significantly restructured the paper. We propose the convergence analysis of asynchronous parallel SGD (APSGD) in Section 4 (previously Section 5) and Section 5 for its comparison with synchronous parallel SGD (SPSGD). Our previously submitted version had these two results mixed—convergence analysis of APSGD and its comparison with SPSGD and was presented jointly under the title “Convergence analysis: Comparison with the synchronous parallel method” in Section 4. This restructuring dissolved many confusions that the reviewers pointed out and increased the readability of the results. Below we address the reviewer’s comments in detail as they appear in the report. 

\paragraph*{Major comments.}
\begin{enumerate}
\item {\it  The analysis holds only when the recurrence is stable and does not hold when the delay is time-varying. {More generally a uniform bound on the eigenvalue magnitudes does not obviously give a rate of convergence in the time-varying case.}}

\textbf{Answer.} 
We sincerely thank the reviewer for this insightful comment. We note that we only require a bounded delay for the convergence analysis of APSGD. The key result which helps us achieve this is Lemma 6 in the current version of the paper~(Lemma 5 in the previous version), which states that the bound on eigenvalues of the state transition matrix $A$ is monotone in delay. Specifically, the rate is monotone in delay---the higher the delay, the higher the bound is. This allows us to simply upper bound an arbitrary delay's rate by the rate for maximum $\delta_a$ delay. Lemma 6 is now a fundamental result in establishing the convergence of APSGD.
Moreover, the convergence analysis of APSGD is now a standalone result that can handle any time-varying bounded delay that the reviewer was concerned. To elaborate more, the convergence analysis of our proposed APSGD can handle any variable delay, $\dt$ bounded by a maximum delay of $\da$. Additionally, we have now added an ``Overview of our analysis" to help the reader better understand the recurrence notion that appears in the form of a state-transition system and our proof techniques that at places become rigorous. To conclude, by disentangling the convergence analysis of APSGD and its comparison with SPSGD, the results in Section 5 now require only a bounded delay $\delta_a$ that is the upper bound of any time-varying delay as mentioned by the reviewer.
\end{enumerate}

\paragraph*{Minor comments}

\begin{enumerate}

\item{\it The authors describe a “unit time interval” to mean the maximum delay for all processors to report $(\delta_a)$; since time is measured in terms of updates at the master, $\delta_a>\tau$ (the number of processors), so a “unit time interval” does not actually correspond to a unit of time. I would recommend choosing different terminology.}

\textbf{Answer.} We thank the reviewer for pointing this out.~We have removed using ``unit time interval" and now clearly state our setup to compare APSGD and SPSGD and justify why this setup is meaningful.
We agree with the reviewer that the notion of the ``unit-time interval" that we were considering in our previous analysis to compare the iteration complexities was confusing. By removing the idea of ``unit-time interval" in our present version, we alleviate this issue. As mentioned before, by eliminating the notion of ``unit time interval" and disentangling the analysis of APSGD and its comparison with SPSGD, the convergence of APSGD can handle {\em any time-varying bounded delay}. Although analyzing different delays of APSGD under diverse setups is a mathematically complicated problem and not in the scope of this paper. 


\item{\it “For the sake of simplicity, we assume the number of updates by the asynchronous parallel SGD in a unit-time interval to be constant throughout the execution of the algorithm”--- this is weaker than the assumption that the authors actually make in their analysis, but is still probably stronger than appropriate. It’s unclear whether arguments made with this assumption would actually generalize to the case of interest (which is just a bounded delay $\delta_a$).}

\textbf{Answer.} 
Any comparison we make between APSGD and SPSGD requires a notion of time equivalence between their updates. Hence, we are required to assume more than just a bounded delay to make any comparison. As we mentioned in Section 5 of the revised version, a completely lock-free APSGD has both {\em intra-iteration} and {\em inter-iteration} gain compared to its synchronous counterpart SPSGD. However, our setting only depicts inter-iteration gain. So, we find our setting more restrictive towards APSGD than what one would expect in practice. Recall that SPSGD takes one step after averaging $\tau$ gradient computations, one by each of the $\tau$ worker.~In contrast, APSGD takes one step after every gradient computation. To make a comparison between SPSGD and APSGD, we assume time equivalence between one step of SPSGD and $\delta_a$ steps of APSGD. Please see Figure 1 added in the manuscript. This setting is a depiction of distributed setups with predictable but variable computing time of the nodes. We note that under this setting, one can consider the {\em intra-iteration gain of APSGD} due to the synchronization barrier after every $\delta_a$ steps. Therefore, APSGD is expected to perform even better without the synchronization barrier, but we do not consider that in our setting. 

\item{\it The use of $S_i$ to denote the number of updates in the given interval from processor $i$ is unfortunate given the use of a bold $S$ elsewhere for the samples drawn at each step; I suggest simply using a different letter. }

\textbf{Answer.} Many thanks for noticing this! We removed this notation in the revised version of our paper.

\item{\it I also found it irritating to flip back and forth to Appendix A for the proofs. None of the proofs is all that long, and I think it would be better to just include them inline. }

\textbf{Answer.} Thank you very much for this suggestion. We have added the proofs inline.



\end{enumerate}


\paragraph*{Text modifications.}
Below we note the major restructuring in the revised version. Major modifications are indicated in \textcolor{blue}{blue} in the revised version. 

\begin{enumerate}
\item{Parallel basic algorithm changed to synchronous parallel SGD~(SPSGD) algorithm in order to change confusion with the basic method. This lead us to APSGD and SPSGD as abbreviations.}
\item We added a simple example to demonstrate the setting to compare between our asynchronous parallel SGD~(APSGD) and synchronous parallel SGD~(SPSGD) proposed by Richt\'{a}rik and Tak\'{a}\v{c} in \cite{richtarik2017stochastic}. 
\item{We have now removed the weak-convergence result~(Previously Section 4.1) and modified the text to only focus on strong convergence. We note that, weak-convergence is a mere consequence of the strong convergence. This keeps the merit of the paper intact.}
\item{For better readability, we added a new Section called ``Overview of analysis"---Section 4.1.}
\item {We propose the convergence analysis of asynchronous parallel SGD~(APSGD) in Section 4 (previously Section 5), and Section 5 is dedicated for its comparison with synchronous parallel SGD (SPSGD). We dispensed the need of a ``unit time interval” from our analysis.}    
\item Tables are presented in a more compact form. 
\item{Newer references were added and rewording of the references in abstract and introduction.}
\end{enumerate}






\bibliographystyle{plain}
\bibliography{../references_asyn}










\end{document}



