\documentclass{article} % For LaTeX2e
%\usepackage{nips_adapted,times}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\mathtoolsset{showonlyrefs} 
\title{Solving Liner Systems}
%{Final Project Report}
\author{
Aashutosh Tiwari \\
\And
Atal Narayan Sahu\\
}

% The \author macro works with any number of authors. There are two commands
% used to separate the names and addresses of multiple authors: \And and \AND.
%
% Using \And between authors leaves it to \LaTeX{} to determine where to break
% the lines. Using \AND forces a linebreak at that point. So, if \LaTeX{}
% puts 3 of 4 authors names on the first line, and the last on the second
% line, try using \AND instead of \And before the third author name.

\newcommand{\fix}{\marginpar{FIX}}
\newcommand{\new}{\marginpar{NEW}}

%\nipsfinalcopy

\begin{document}

\maketitle

\section*{The Main Problem}
The problem we are trying to solve is: (Consistency is assumed)$$Ax=b,\quad \{\text{dim}(A)=m\times n\}$$
Using the iterative technique:$$x_{k+1}=x_k-\omega B^{-1}A^TS_k(S_k^TAB^{-1}A^TS_k)^{\dagger}S_k^T(Ax_k-b)$$
where, B is any $m\times m$ pd matrix and $S_k$'s are random matrices sampled independently from any distribution $\mathcal{D}$ on matrices.\par
For which we know that the method converges linearly with rate \cite{gower2015stochastic} :
$$\rho=1-\lambda_{min}(W)\quad\{\text{strong convergence}\}$$
where, $$W=\mathbb{E}_{S\sim\mathcal{D}}[B^{-1/2}A^TS(S^TAB^{-1}A^TS)^{\dagger}S^TAB^{-1/2}]$$(Existence of expectation and exactness of stochastic formulation is assumed)


\section*{The Specific case we Analyze}
\begin{itemize}
    \item A is $m\times m$ symmetric positive definite matrix.\\
    \item $S_k$'s are simple vectors i.e. $m\times1$ matrix, sampled from a discrete dist $\mathcal{D}$. 
\end{itemize}
Hence the iterations becomes:
$$x_{k+1}=x_k-\omega\frac{ B^{-1}AS_kS_k^T}{S_k^TAB^{-1}AS_k}(Ax_k-b)$$
And for exactness we need $$\mathbb{E}_{S\sim\mathcal{D}}\Bigg[\frac{SS^T}{S^TAB^{-1}A^TS}\Bigg]\succ 0$$
So the problem now is what should be the choice of optimal $S$'s i.e. optimal $\mathcal{D}$.


\section*{Approaching The Problem}
The following work displays the thought process we went through to approach the problem.

\subsection{Eigen vector approach with B=I}
We know that the convergence rate is bounded by: \cite{gower2015stochastic}
\begin{equation}\label{minRho}
    1-\frac{\mathbb{E}_{S\sim\mathcal{D}}[\textbf{Rank}(S^TA)]}{\textbf{Rank}(A)}\leq\rho<1
\end{equation}
\textit{Claim.} If we choose $S$'s as the eigen vectors of A (all $m$ of them exist and are also linearly independent as A is pd) with equal probabilities, we get the best possible convergence rate for this method in the above mentioned conditions, which is:$$\rho=1-\frac{\mathbb{E}_{S\sim\mathcal{D}}[\textbf{Rank}(S^TA)]}{\textbf{Rank}(A)}=1-\frac{1}{m}$$
\textit{Proof.}  Let $S^j$'s (the vectors random vector $S$ can be) be the orthonormal eigen vectors of A with corresponding eigen values as $\lambda^j$'s where $\textit{j}=1:1:m$.\\ 
Taking $B=I$,
\begin{equation}\label{Wk}
   B^{-1/2}A^TS(S^TAB^{-1}A^TS)^{\dagger}S^TAB^{-1/2}= A^TS(S^TAA^TS)^{\dagger}S^TA
\end{equation}
As $A$ is symmetric, for a particular eigenvector $S^j$, we have $A^TS^j=AS^j=\lambda^jS^j.$\\
thus (\eqref{Wk}) for $S=S^j$, now becomes\\
$$\frac{(\lambda^j)^2S^j(S^j)^T}{(\lambda^j)^2}={S^j(S^j)^T}$$
If we sample all the $m$ eigenvalues with equal probabilities $p_i=\frac{1}{m}$, then \\
$$W=\frac{1}{m}\sum_{j=1}^mS^j(S^j)^T=\frac{1}{m}UU^T=\frac{1}{m}I$$
where U is the ortho-normal matrix of eigen vectors $S^j$'s.\\
$W$ is a diagonal matrix with all diagonal entries as $\frac{1}{m}$, thus $\lambda_{min}(W)=\frac{1}{m}$.\\
which gives $$\rho=1-\lambda_{min}(W)=1-\frac{1}{m}$$\\
But the problem with this approach is that we have to compute the eigen vectors of A which is expensive.

\subsection{Right singular vectors for partial matrices of $A$ with $B=I$}
To overcome the problem of large time taken in computing eigen vectors of A we try to use $S$'s as right singular vectors (corresponding to non-zero singular value) of partial matrices of $A$, i.e. we partition almost all of A in chunks of 2-rows each and let's call them $A_i$,
$$A=\begin{bmatrix}
A_1\\
A_2\\
\vdots\\
A_{\lceil{m/2}\rceil}
\end{bmatrix}$$ 
And we know each of $A_i$ can be written in SVD as:
\begin{equation}\label{svd}
A_i=U_i\Sigma_iV_i^T    
\end{equation}
where $U_i$ is the orthonormal matrix of eigen vectors of $A_iA_i^T$,
$$A_iA_i^T=U_i\Lambda_iU_i^T$$ $$U_i=[U_i^1U_i^2]$$ 
and $V_i$ is the orthonormal matrix of eigen vectors of $A_i^TA_i$,
$$A_i^TA_i=V_i\Lambda'_iV_i^T$$
$$V_i=[V_i^1,\hdots,V_i^m]$$
Also, $\Lambda_i=\Sigma_i\Sigma_i^T$ and $\Lambda'_i=\Sigma_i^T\Sigma_i$, $\Sigma_i$ ($2\times m$ or $1\times m$) contains non-zero singular values of $A_i$ on it's diagonals, let's call them $\sigma_i^1$ and $\sigma_i^2$ (or just $\sigma_i^1$).
\\
Using \eqref{svd} we can see,
\begin{align*}
A_iV_i&=U_i\Sigma_i\\
A_iV_i^1&=U_i^1\sigma_i^1\\
A_iV_i^2&=U_i^2\sigma_i^2\\
A_iV_i^j&=\textbf{0}\ \ \forall j\in [3:1:m]
\end{align*}
So let,
\begin{align*}
    &\quad\quad S\in\Bigg\{\bigcup_j\bigcup_{i=1}^2V_i^j\Bigg\}\\
    \implies&\frac{AV_i^j(V_i^j)^TA}{(V_i^j)^TAAV_i^j}
    =\frac{
        \begin{bmatrix}
            \sigma_i^jU_i^j\\A_2V_i^j\\\vdots\\A_{\lceil{m/2}\rceil}V_i^j
        \end{bmatrix}
        \begin{bmatrix}
            \sigma_i^j(U_i^j)^T\ (A_2V_i^j)^T\ \hdots\ (A_{\lceil{m/2}\rceil}V_i^j)^T
        \end{bmatrix}
    }{||A_2V_i^j||^2+\hdots+||A_{\lceil{m/2}\rceil}V_i^j||^2}\\
    &\quad\quad=\frac{\begin{bmatrix}
        (\sigma_i^j)^2U_i^j(U_i^j)^T&\sigma_i^jU_i^j(V_i^j)^TA_2^T &\hdots &\sigma_i^jU_i^j(V_i^j)^TA_{\lceil{m/2}\rceil}^T\\
        \sigma_i^jA_2V_i^j(U_i^j)^T& A_2V_i^j(V_i^j)^TA_2^T&\hdots&A_2V_i^j(V_i^j)^TA_{\lceil{m/2}\rceil}^T\\
        \vdots&\vdots&\vdots&\vdots\\
        \sigma_i^jA_{\lceil{m/2}\rceil}V_i^j(U_i^j)^T&A_{\lceil{m/2}\rceil}V_i^j(V_i^j)^TA_2^T&\hdots&A_{\lceil{m/2}\rceil}V_i^j(V_i^j)^TA_{\lceil{m/2}\rceil}^T
    \end{bmatrix}
    }{(\sigma_i^j)^2+||A_2V_i^j||^2+\hdots+||A_{\lceil{m/2}\rceil}V_i^j||^2}
\end{align*}
Now we can choose our probabilities as $\frac{(\sigma_i^j)^2+||A_2V_i^j||^2+\hdots+||A_{\lceil{m/2}\rceil}V_i^j||^2}{P}$, where $P=\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2(\sigma_i^j)^2+||A_2V_i^j||^2+\hdots+||A_{\lceil{m/2}\rceil}V_i^j||^2$. Still the problem does not seem to get simplified much so we'll look at a simplified version in subsection 5 below.

%$$W=\frac{1}{P}\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2\begin{bmatrix}(\sigma_i^j)^2U_i^j(U_i^j)^T &\sigma_i^jU_i^j(V_i^j)^TA_2^T &\hdots &\sigma_i^jU_i^j(V_i^j)^TA_{\lceil{m/2}\rceil}^T\\
 %   \sigma_i^jA_2V_i^j(U_i^j)^T&A_2A_2^T&\hdots&A_2A_{\lceil{m/2}\rceil}^T\\
  %  \vdots&\vdots&\vdots&\vdots\\
   % \sigma_i^jA_{\lceil{m/2}\rceil}V_i^j(U_i^j)^T&A_{\lceil{m/2}\rceil}A_2%^T&\hdots&A_{\lceil{m/2}\rceil}A_{\lceil{m/2}\rceil}^T\end{bmatrix}\\
%    $$ $$\implies W=\frac{AA^T}{P}$$
 %   $$\implies\rho=1-\frac{(\lambda_{min}(A))^2}{P}$$
%Note that we are just using the first two right eigen vector of every $A_i$ as rest of them corresponds to zero eigen value and hence gives zero on multiplying with $A_i$\\

\subsection{Left singular vectors for partial matrices of $A$ with $B=I$}
Let us now consider another discrete distribution $D$ consisting of the following $m$ vectors $S_i^j$ %where $\textit{i}=1:1:\lceil{m/2}\rceil$ and  $\textit{j}=1:1:2$, where
\begin{equation}\label{def Sij}
    S_i^j=\begin{bmatrix}
0\\
\vdots\\
0\\
U_i^j\\
0\\
\vdots\\
0\\
\end{bmatrix}
\end{equation}
Where $U_i^j$ is placed in corresponding block of $A_i$.\\
We sample with probability,
\begin{equation}\label{prob left I}
    {p_i^j}=\frac{(\sigma_i^j)^2}{\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2{(\sigma_i^j)^2}}
\end{equation}
We have,
\begin{equation}\label{prod Sij}
    (S_i^j)^TA=(U_i^j)^TA_i=\sigma_i^j(V_i^j)^T
\end{equation}
%Let $W =\mathbb{E}_{S_i^j\sim\mathcal{D}}[W_i^j]\\$
%From $\eqref{Wk}$, we have
\begin{equation}\label{WkleftI}
    \implies A^TS_i^j((S_i^j)^TAA^TS_i^j)^{\dagger}(S_i^j)^TA=\frac{{\sigma_i^j}^2{V_i^jV_i^j}^T}{(\sigma_i^j)^2}=V_i^j{V_i^j}^T
\end{equation}
And,
\begin{equation}\label{prod2}
  \sum_{j=1}^2(\sigma_i^j)^2V_i^j{V_i^j}^T=A_iA_i^T
\end{equation}
As $(\sigma_i^1)^2$ and $(\sigma_i^2)^2$ are eigenvalues of $A_iA_i^T$,we have, for any $A_i$,\cite{cmu}
\begin{equation}\label{sigmasquare}
  \sum_{j=1}^2(\sigma_i^j)^2=||A_i||^2_F 
\end{equation}
Thus,
\begin{equation}\label{sigmasquaresum}
  \sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2(\sigma_i^j)^2=\sum_{i=1}^{\lceil{m/2}\rceil}||A_i||^2_F=||A||^2_F
\end{equation}
\begin{equation}\label{WleftI}
  W=\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2p_j^iA^TS_i^j((S_i^j)^TAA^TS_i^j)^{\dagger}(S_i^j)^TA=\frac{\sum_iA_iA_i^T}{\sum_i\sum_j(\sigma_i^j)^2}=\frac{AA^T}{||A||^2_F}
\end{equation}
And the convergence rate $\rho$ is,
\begin{equation}\label{rholeftI}
  \rho=1-\frac{\lambda_{min}(A^2)}{||A||^2_F}=1-(\frac{\lambda_{min}(A)}{||A||_F})^2
\end{equation}
\subsection{Left singular vectors for partial matrices of $A$ with $B=A$}
We know that  $W=B^{-1/2}\mathbb{E}[Z]B^{-1/2}$ and $Q=B^{-1}\mathbb{E}[Z]$ have the same eigenvalues, where \\ $Z=A^TS(S^TAB^{-1}A^TS)^{\dagger}S^TA$, hence
\begin{equation}\label{lambdaequ}
  \lambda_{min}(W)=\lambda_{min}(Q)
\end{equation}
Let $S_i^j$ be the same as defined in $(\eqref{def Sij})$. We sample them with probability
\begin{equation}\label{prob left A}
    {p_i^j}=\frac{{S_i^j}^TAS_i^j}{{\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2{{S_i^j}^TAS_i^j}}}
\end{equation}
Let 
\begin{equation}\label{def Zij}
  Z_i^j=A^TS_i^j({S_i^j}^TAB^{-1}A^TS_i^j)^{\dagger}{S_i^j}^TA
\end{equation}
\begin{equation}\label{def Qij}
  Q_i^j=B^{-1}Z_i^j
\end{equation}
Taking $B=A$ and $A=A^T$, from (\eqref{def Zij}), (\eqref{def Qij}) and (\eqref{prod Sij}),
we get,
\begin{equation}\label{equ Qij}
  Q_i^j=S_i^j({S_i^j}^TAS_i^j)^{\dagger}{S_i^j}^TA=\frac{S_i^j\sigma_i^j{V_i^j}^T}{{S_i^j}^TAS_i^j}
\end{equation}
Thus, from (\eqref{prob left A})
\begin{equation}\label{equ Q}
  Q=\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2p_i^j Q_i^j=\frac{1}{{\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2{{S_i^j}^TAS_i^j}}}{{\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2S_i^j\sigma_i^j{V_i^j}^T}}
\end{equation}
Now,
\begin{equation}\label{num left A}
  S_i^j\sigma_i^j{V_i^j}^T=\begin{bmatrix}
0\\
\vdots\\
0\\
U_i^j\sigma_i^j{V_i^j}^T\\
0\\
\vdots\\
0\\
\end{bmatrix}
\end{equation}
And,
\begin{equation}\label{sum left A}
  \sum_{j=1}^2S_i^j\sigma_i^j{V_i^j}^T=\begin{bmatrix}
0\\
\vdots\\
0\\
U_i^1\sigma_i^1{V_i^1}^T+U_i^2\sigma_i^2{V_i^2}^T\\
0\\
\vdots\\
0\\
\end{bmatrix}
=\begin{bmatrix}
0\\
\vdots\\
0\\
A_i\\
0\\
\vdots\\
0\\
\end{bmatrix}
\end{equation}
Hence,
\begin{equation}\label{num left Q A}
  \sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2S_i^j\sigma_i^j{V_i^j}^T=
  \begin{bmatrix}
A_1\\
\vdots\\
A_{\lceil{m/2}\rceil}\\
\end{bmatrix}
=A
\end{equation}
And Q becomes,
\begin{equation}\label{left Q A}
  Q=\frac{A}{\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2{{S_i^j}^TAS_i^j}}
\end{equation}
Now, since ${S_i^j}^TAS_i^j$ is scalar, we get
\begin{equation}\label{xTax}
  {S_i^j}^TAS_i^j=tr({S_i^j}^TAS_i^j)=tr(AS_i^j{S_i^j}^T)
\end{equation}
And,
\begin{equation}\label{sum Ui}
  \sum_{j=1}^2S_i^j{S_i^j}^T=\begin{bmatrix}
 0&\hdots&0\\
 \vdots&\sum_{j=1}^2U_i^j{U_i^j}^T&\vdots\\
 0&\hdots&0
 \end{bmatrix}\\
 =\begin{bmatrix}
 0&\hdots&0\\
 \vdots&U_iU_i^T&\vdots\\
 0&\hdots&0
 \end{bmatrix}\\
 =\begin{bmatrix}
 0&\hdots&0\\
 \vdots&I_{2\text{x}2}&\vdots\\
 0&\hdots&0
 \end{bmatrix}\\
\end{equation}
where 1 is in $(2i-1^{th})$ and 
The rate of convergence $\rho$ is,
\begin{equation}\label{rhoLeftA}
  \rho=1-\lambda_{min}(Q)=1-\frac{\lambda_{min}(A)}{\sum_{i=1}^{\lceil{m/2}\rceil}\sum_{j=1}^2{S_i^j}^TAS_i^j}
\end{equation}

\subsection{Taking coarser partitions}
Let us divide the A matrix in four parts of almost equal sizes:$$A=\begin{bmatrix}
A_{11}&A_{12}\\A_{21}&A_{22}
\end{bmatrix}$$
keeping $A_{11}$ and $A_{22}$ square.
$$A_{11}=X_1\Lambda_1X_1^T$$$$A_{22}=X_2\Lambda_2X_2^T$$
now let's choose $S$'s as:
$$S\in\Bigg\{\begin{bmatrix}X_1^j\\ 0\\\vdots\\0\end{bmatrix}\Bigg\}\bigcup\Bigg\{\begin{bmatrix}0\\\vdots\\ 0\\ X_2^j\end{bmatrix}\Bigg\}\quad\\$$% i,j=1:1:\lceil{m/2}\rceil\\
Where $X_i^j$ is the $j^{th}$ column of $X_i$ i.e. eigen vector of $A_{ii}$.
$$\implies AS=\begin{bmatrix}\lambda_1^jX_1^j\\A_{21}X_1^j\end{bmatrix}\text{or}\begin{bmatrix}A_{12}X_2^j\\\lambda_2^jX_2^j\end{bmatrix}$$
\begin{align*}
 \implies\frac{B^{-1}AS(AS)^T}{(AS)^TB^{-1}AS}=&\frac{\begin{bmatrix}(\lambda_1^j)^2X_1^j(X_1^j)^T&\lambda_1^jX_1^j(X_1^j)^TA_{21}^T\\
 \lambda_1^j A_{21} X_1^j (X_1^j)^T & A_{21}X_1^j(X_1^j)^TA_{21}^T
 \end{bmatrix}}{(\lambda_1^j)^2+(X_1^j)^TA_{21}^TA_{21}X_1^j}\\
 \text{or }&\frac{\begin{bmatrix} A_{12}X_2^j(X_2^j)^TA_{12}^T&A_{12}\lambda_2^jX_2^j(X_2^j)^T\\
 \lambda_2^jX_2^j(X_2^j)^TA_{12}^T&(\lambda_2^j)^2X_2^j(X_2^j)^T
 \end{bmatrix}}{(\lambda_2^j)^2+(X_2^j)^TA_{12}^TA_{12}X_2^j}\quad(\text{For }B=I)\\
 =&\frac{1}{\lambda_1^j}\begin{bmatrix}\lambda_1^jX_1^i(X_1^j)^T& &X_1^j(X_1^j)^TA_{21}^T\\
 0&\hdots&0\\
 \vdots&\vdots&\vdots\\
 0&\hdots&0
 \end{bmatrix}\\
 \text{or }&\frac{1}{\lambda_2^j}\begin{bmatrix}
 0&\hdots&0\\
 \vdots&\vdots&\vdots\\
 0&\hdots&0\\
 A_{12}^TX_2^j(X_2^j)^T& &\lambda_2^jX_2^j(X_2^j)^T
 \end{bmatrix}\quad(\text{For }B=A)\\
\end{align*}
\begin{align*}
 (\text{For }B=I,\text{probablity}=&\frac{(\lambda_i^j)^2+(X_i^j)^TA_{(3-i)i}^TA_{(3-i)i}X_i^j}{P}, P=\sum_j\sum_{i=1}^2(\lambda_i^j)^2+(X_i^j)^TA_{(3-i)i}^TA_{(3-i)i}X_i^j)\\
 \implies \mathbb{E}\Bigg[\frac{B^{-1}AS(AS)^T}{(AS)^TB^{-1}AS}\Bigg]=&\frac{1}{P}\begin{bmatrix}A_{11}A_{11}+A_{12}A_{12}^T&A_{11}A_{21}^T+A_{12}A_{22}\\
 A_{21}A_{11}+A_{22}A_{12}^T&A_{21}A_{21}^T+A_{22}A_{22}
 \end{bmatrix}=\frac{AA^T}{P}\\
 (\text{For }B=I,\text{probablity}=&\frac{(\lambda_i^j)^2+(X_i^j)^TA_{(3-i)i}^TA_{(3-i)i}X_i^j}{(\lambda_i^j)^2P}, P=\sum_j\sum_{i=1}^2\frac{(\lambda_i^j)^2+(X_i^j)^TA_{(3-i)i}^TA_{(3-i)i}X_i^j)}{(\lambda_i^j)^2}\\
 \mathbb{E}\Bigg[\frac{B^{-1}AS(AS)^T}{(AS)^TB^{-1}AS}\Bigg]=&\frac{1}{P}\begin{bmatrix}A_{12}A_{22}^{-2}A_{12}^T&A_{11}^{-1}A_{21}^T+A_{12}A_{22}^{-1}\\
 A_{21}A_{11}^{-1}+A_{22}^{-1}A_{12}^T&A_{21}A_{11}^{-2}A_{21}^T
 \end{bmatrix}\\
 =&\frac{A}{\textbf{Tr}(A)}(\text{For }B=A,\text{ probabilities}=\frac{\lambda_i^j}{\sum_j\sum_{i=1}^{2}\lambda_i^j})\\
 =&\frac{1}{m}\begin{bmatrix}I&A_{11}^{-1}A_{21}^T\\
 A_{12}^TA_{22}^{-1}&I
 \end{bmatrix}(\text{For }B=A,\text{ equal probabilities}=\frac{1}{m})
\end{align*}

~
% \subsubsection*{References}
% % may use bixtex to automatically generate the list of references or
% % enter these manually below
% 
% \small{
% [1] Reference 1
% 
% [2] Reference 2
% }
\bibliography{ref}
\bibliographystyle{plain}


\end{document}