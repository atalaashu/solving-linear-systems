clear;m=100;
[t,l] = meshgrid(0:1/m:1,0:1/m:1);
t=triu(t);l=triu(l);l(t==0)=NaN;t(t==0)=NaN;
tau=5;p=zeros(1,tau+2);p(1)=1;
z3=zeros(size(t));
for i=1:m+1
    for j=1:m+1
        p(2)=-(1-(i-1)/m)*(1-(j-1)/m);p(tau+2)=-(i-1)*(1-(j-1)/m)/m;
        z3(j,i)=(max(roots(p)));
    end
end
z3=abs(z3);
colormap([1 0 0;1 1 1;0 0 1;0 1 1])
z3n=ones(size(t))./(ones(size(t))-z3);
z3n(1,:)=NaN;
z5n=ones(size(l))./l;
z6n=ones(size(t))./(ones(size(t))-(ones(size(t))-l).^(1/(tau+1)));
figure(2);hold on;
surf(t,l,z3n,ones(size(l))+2);
surf(t,l,z5n,ones(size(l))+1);
surf(t,l,z6n,ones(size(l))+3);
hold off;xlabel('t');ylabel('l');