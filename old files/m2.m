clear;close all;m=100;
[a,b] = meshgrid(0:1/m:1,0:1/m:1);
for i=1:101
    for j=102-i:101
        a(i,j)=NaN;
        b(i,j)=NaN;
    end
end 
tau=5;p=zeros(1,tau+2);p(1)=1;
z1=(a+b).^(1/(1+tau));
z1n=ones(size(a))./(ones(size(a))-z1);
o=.5;z2=max(a/o,(b/(1-o)).^(1/(tau+1)));
z2n=ones(size(a))./(ones(size(a))-z2);
z3=zeros(size(a));
for i=1:m+1
    for j=1:m+1
        p(2)=-(i-1)/m;p(tau+2)=-(j-1)/m;
        z3(j,i)=(max(roots(p)));
    end
end
z3(:,1)=abs(z3(:,1));
z3n=ones(size(a))./(ones(size(a))-z3);
z4=(a+b);%.^(1/tau);
z4n=ones(size(a))./(ones(size(a))-z4);
colormap([1 0 0;1 1 1;0 0 1;0 1 1])
figure(1);surf(a,b,z1,ones(size(b)));hold on;surf(a,b,z2,ones(size(b))+1);surf(a,b,z3,ones(size(b))+2);
%surf(a,b,z4,ones(size(b))+3);
hold off;xlabel('a');ylabel('b');
figure(2);hold on;
surf(a,b,z1n,ones(size(b)));
for o=0.5:0.1:0.9
    z2=max(a/o,(b/(1-o)).^(1/(tau+1)));
    z2n=ones(size(a))./(ones(size(a))-z2);
    surf(a,b,z2n,ones(size(b))+1);
end
surf(a,b,z3n,ones(size(b))+2);
surf(a,b,z4n,ones(size(b))+3);
hold off;xlabel('a');ylabel('b');
