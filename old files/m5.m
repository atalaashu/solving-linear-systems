clear;m=50;
[t,w] = meshgrid(0:1/m:1,0:1/m:5);
tau=5;p=zeros(1,tau+2);p(1)=1;
z3=zeros(size(t));l=.2;
colormap([1 0 0;1 1 1;0 0 1;0 1 1])
for l=0.2:0.2:0.8
for i=1:m+1
    for j=1:5*m+1
        p(2)=-(1-(i-1)/m)*(1-(i-1)*(j-1)*l/m^2);
        p(tau+2)=-((i-1)/m-(i-1)*(j-1)*l/m^2-(i-1)^2*(j-1)*l/m^3+(i-1)^2*(j-1)^2*l/m^4);
        z3(j,i)=max(roots(p));
    end
end
z3=abs(z3);
z3n=ones(size(t))./(ones(size(t))-z3);
z3n(1,:)=NaN;
z5n=ones(size(t))/l;
z6n=ones(size(t))/(1-(1-l)^(1/(tau+1)));
z3n(z3n>100000)=0;z3n(z3n<-100000)=0;
figure(1);hold on;
surf(t,w,z3n,ones(size(t))+2);
surf(t,w,z5n,ones(size(t))+1);
surf(t,w,z6n,ones(size(t))+3);
end
hold off;xlabel('t');ylabel('w');