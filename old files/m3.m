clear;close all;m=100;
[a,b] = meshgrid(0:1/m:1,0:1/m:1);
for i=1:101
    for j=102-i:101
        a(i,j)=NaN;
        b(i,j)=NaN;
    end
end 
z3=zeros(size(a));z2n=zeros(size(a));z3n=zeros(size(a));z1n=zeros(size(a));
colormap([1 0 0;1 1 1;0 0 1;0 1 1])
for tau=5:10:50
    z1=(a+b).^(1/(1+tau));
    z1n(:,:,ceil(tau/10)+1)=ones(size(a))./(ones(size(a))-z1);
    z2=max(2*a,(2*b).^(1/(tau+1)));
    z2n(:,:,ceil(tau/10)+1)=ones(size(a))./(ones(size(a))-z2);
    p=zeros(1,tau+2);p(1)=1;
    for i=1:m+1
        for j=1:m+1
            p(2)=-(i-1)/m;p(tau+2)=-(j-1)/m;
            z3(i,j)=(max(roots(p)));
        end
    end
    z3(1,:)=abs(z3(1,:));
    z3n(:,:,ceil(tau/10)+1)=ones(size(a))./(ones(size(a))-z3);
    for i=1:m+1
        z3n(m+2-i,i,ceil(tau/10)+1)=0;
    end
    surf(a,b,z1n(:,:,ceil(tau/10)+1),ones(size(b)));hold on;
    surf(a,b,z2n(:,:,ceil(tau/10)+1),ones(size(b))+1);
    surf(a,b,z3n(:,:,ceil(tau/10)+1)',ones(size(b))+2);
    xlabel('a');ylabel('b');
end
z4=(a+b);%.^(1/tau);
z4n=ones(size(a))./(ones(size(a))-z4);
surf(a,b,z4n,ones(size(b))+3);